import 'package:flutter/material.dart';

class Homepage extends StatefulWidget {
  const Homepage({super.key, required this.title});
  final String title;
  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  final List<int> _fruits = [];
  int _counter = 0;

  void _onPressed() {
    setState(() {
      _counter++;

      _fruits.add(_counter);
    });
  }

  bool _isPrime(int nb) {
    for (var i = 2; i <= nb / i; ++i) {
      if (nb % i == 0) {
        return false;
      }
    }
    return true;
  }

  ListTile _primeTile(int nb) {
    return ListTile(
      leading: Image.asset('images/ananas.png'),
      title: Text(nb.toString()),
      tileColor: nb.isEven ? Colors.indigo : Colors.cyan,
      textColor: Colors.white,
    );
  }

  ListTile _evenTile(int nb) {
    return ListTile(
      leading: Image.asset('images/poire.png'),
      title: Text(nb.toString()),
      tileColor: Colors.indigo,
      textColor: Colors.white,
    );
  }

  ListTile _oddTile(int nb) {
    return ListTile(
      leading: Image.asset('images/pomme.png'),
      title: Text(nb.toString()),
      tileColor: Colors.cyan,
      textColor: Colors.white,
    );
  }

  Widget _buildFruit(int nb) {
    return _isPrime(nb)
        ? _primeTile(nb)
        : nb.isEven
            ? _evenTile(nb)
            : _oddTile(nb);

    // if (number.isEven) {
    //   return Text("pair");
    // } else {
    //   return ListTile(
    //     title: Text(number.toString()),
    //   );
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(_isPrime(_counter)
              ? _counter.isEven
                  ? '$_counter : Nombre premier et pair' : '$_counter : Nombre premier et impair'
              : _counter.isEven
                  ? '$_counter : Nombre non premier et pair' : '$_counter : Nombre non premier et impair')),
      body: Center(
        child: ListView.builder(
          itemCount: _fruits.length,
          itemBuilder: ((BuildContext context, int index) {
            return _buildFruit(_fruits[index]);
          }),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: _counter == 0 ? Colors.blue : Colors.red,
          onPressed: _onPressed,
          child: const Icon(Icons.add)),
    );
  }
}

class Fruit {
  final String name;

  Fruit({required this.name});
}
