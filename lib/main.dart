import 'package:flutter/material.dart';
import 'package:pomme_poire_ananas/homepage.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Homepage(title: 'Liste de fruits')
    );
  }
}

class Fruit {
  final String name;

  Fruit({required this.name});
}